<?php
/**
 * email header
 *
 * @version	1.0
 * @since 1.4
 * @package	Wordpress Social Invitations
 * @author Timersys
 */
if ( ! defined( 'ABSPATH' ) ) exit; 


$wrapper = "
	background-color:".$settings['body_bg'].";
	width:100%;
	-webkit-text-size-adjust:none !important;
	margin:0;
	padding: 70px 0 70px 0;
";
$border_radius = $settings['template'] == 'boxed' ? '44px' : '0px';
$template_container = "
	-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	-webkit-border-radius:$border_radius !important;
	border-radius:$border_radius !important;
	background-color: #fafafa;
	border-radius:44px !important;
	width: 100%;
	max-width: ". ($settings['template'] == 'boxed' ? $settings['body_size'].'px' : '100%') .";";
$template_header = "
	background-color: ".$settings['header_bg'].";
	color: #f1f1f1;
	-webkit-border-top-left-radius:$border_radius !important;
	-webkit-border-top-right-radius:$border_radius !important;
	border-top-left-radius:$border_radius !important;
	border-top-right-radius:$border_radius !important;
	border-bottom: 40;
	font-family:Arial;
	font-weight:bold;
	line-height:100%;
	vertical-align:middle;
";
$body_content = "
	background-color: ".$settings['email_body_bg'].";
";
$body_content_inner = "
	color: ".$settings['body_text_color'].";
	font-family:Arial;
	font-size: ".$settings['body_text_size']."px;
	line-height:150%;
	text-align:left;
";
$header_content_h1 = "
	color: ".$settings['header_text_color'].";
	margin:0;
	padding: 28px 24px;
	display:block;
	font-family:Arial;
	font-size: ".$settings['header_text_size']."px;
	font-weight:bold;
	text-align:".$settings['header_aligment'].";
	line-height: 150%;
";
$header_content_h1_a = "
	color: ".$settings['header_text_color'].";
	text-decoration: none;
";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo get_bloginfo('charset');?>" />
        <title><?php echo get_bloginfo('name'); ?></title>
	    <style type="text/css">
		    #template_body a{
			    color: <?= $settings['body_href_color'];?>;
		    }
	    </style>
	    <style type="text/css" id="custom-css">
		    <?= $settings['custom_css'];?>
	    </style>
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<div id="body" style="<?php echo $wrapper; ?>">
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            	<tr>
                	<td align="center" valign="top">
                    	<table border="0" cellpadding="0" cellspacing="0"  id="template_container" style="<?php echo $template_container; ?>">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- Header -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header" style="<?php echo $template_header; ?>">
                                        <tr>
                                            <td>
                                            	<h1 style="<?php echo $header_content_h1; ?>" id="logo">
		                                            <a style="<?php echo $header_content_h1_a;?>" href="<?php echo apply_filters( 'mailtpl/templates/header_logo_url', home_url());?>" title="<?php echo apply_filters( 'mailtpl/templates/header_logo_url_title', !empty($settings['header_logo_text']) ? do_shortcode( strip_tags($settings['header_logo_text']) ) : get_bloginfo('name') );?>"><?php
		                                            if( !empty($settings['header_logo']) ) {

		                                            	$attrs = apply_filters( 'mailtpl/templates/header_logo_attr', array('style' => 'max-width:100%;'));

		                                            	$header_logo_attr = [];
		                                            	foreach($attrs as $attr_key => $attr_value)
		                                            		$header_logo_attr[] = sanitize_key($attr_key).'='.sanitize_text_field($attr_value);

			                                            echo '<img '.implode(' ', $header_logo_attr).' src="'.apply_filters( 'mailtpl/templates/header_logo', $settings['header_logo'] ).'" alt="'. apply_filters( 'mailtpl/templates/header_logo_alt', !empty($settings['header_logo_text']) ? do_shortcode( strip_tags($settings['header_logo_text']) ) : get_bloginfo( 'description' ) ) .'"/>';
		                                            } elseif ( !empty( $settings['header_logo_text'] ) ) {
														echo do_shortcode($settings['header_logo_text']);
		                                            } else {
														echo get_bloginfo('name');
		                                            }  ?>
		                                            </a>
	                                            </h1>

                                            </td>
                                        </tr>
                                    </table>
									<table class="m_-3878780116491185642row m_-3878780116491185642header-v2" style="background-color:#fff;background-image:none;background-position:top left;background-repeat:repeat;border-bottom:1px solid #bcbcbc;border-collapse:collapse;border-spacing:0;display:table;margin:20px 0 15px 0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
										<th class="m_-3878780116491185642small-12 m_-3878780116491185642columns" style="Margin:0 auto;color:#322f37;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0!important;padding-left:20px;padding-right:20px;padding-top:0!important;text-align:left;width:560px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#322f37;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
											<a href="" style="Margin:0;color:#57b846;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.twitch.tv/r/e/eyJjaGFubmVsIjoidGgzYW50b25pbyIsImxvY2F0aW9uIjoibG9nbyIsImVtYWlsX2lkIjoiYTE1NjEyNmItMTg2My00ZGQ4LTg4NDItMjgxZmE2MDg5NTBjIiwic291cmNlX2VtYWlsIjoidHdpdGNoX2Zhdm9yaXRlX3VwIiwibmFtZSI6InR3aXRjaF9mYXZvcml0ZV91cCIsImxvZ2luIjoiIn0%3D/114707970/8e91ec9dc5af11d18be1a9414e9b1a9df2d0b02d?ignore_query%3Dtrue%26tt_content%3Dtwitch_favorite_up%26tt_email_id%3Da156126b-1863-4dd8-8842-281fa608950c%26tt_medium%3Demail&amp;source=gmail&amp;ust=1594347500959000&amp;usg=AFQjCNFDZZU3uYTY6XuC-Uq19_G38F5t0w">
												<center><img style=" max-width: 100%; width: 400px;" src="https://image.freepik.com/vector-gratis/personas-comprando-linea_24908-55864.jpg" alt="IMG">
												</center>
											</a>
										</th>
											<th style="Margin:0;color:#322f37;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;width:0"></th></tr></tbody></table></th>
										</tr>
										</tbody>
									</table>

                                    <!-- End Header -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- Body -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_body">
                                    	<tr>
                                            <td valign="top" style="<?php echo $body_content; ?>" id="mailtpl_body_bg">
                                                <!-- Content -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <div style="<?php echo $body_content_inner; ?>" id="mailtpl_body">