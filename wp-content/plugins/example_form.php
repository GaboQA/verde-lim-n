<?php
/**
*Plugin Name: Example Form Plugin 
 */

 function example_form_plugin(){
    $content = '';
    $content .= '<h2>Contactanos</h2>';
    $content .= '<form method="post" action="">';
    $content .= '<label>Nombre</label>';
    $content .= '<input type="text" name="your_name" class="form-control" placeholder="Ingresa tu nombre"/>';
    $content .= '<label for="your_email">Email</label>';
    $content .= '<input type="email" name="your_email" class="form-control" placeholder="Ingresa tu correo electronico"/>';
    $content .= '<label for="your_comments">Preguntas y comentarios</label>';
    $content .= '<textarea name="your_comments" class="form-control" placeholder="Ingresa tu pregunta"></textarea>';
    $content .= '<br /><input type="submit" name="example_form_submit" class="btn btn-md btn-primary" value="Envia tu informacion"/>'; 
    $content .= '</form>';
    return $content;
 }
 add_shortcode('example_form','example_form_plugin');
 //Para imprimir algo se hace con print_r
 function example_form_captura(){
     if(isset($_POST['example_form_submit']))
     {
        $name = sanitize_text_field($_POST['your_name']);
        $email = sanitize_text_field($_POST['your_email']);
        $comments = sanitize_textarea_field($_POST['your_comments']);
            
        $to = 'gabrielqqquesada@gmail.com';
        $subject = 'Test form submission';
        $message = ''.$name.' - '.$email.' - '.$comments;
        wp_mail($to,$subject,$message);


     }
 }
 add_action('wp_head', 'example_form_captura');
 
 ?>
