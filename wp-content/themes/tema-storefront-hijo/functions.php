<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'storefront-gutenberg-blocks' ) );
    }
endif;

add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

/*Estilos*/
function estilos_login(){
    wp_enqueue_style('login-personal-css', site_url(). '/wp-content/themes/tema-storefront-hijo/css/login.css', array(), '5.5');
}
add_action('login_head', 'estilos_login');

//Funcion para devolverse al inicio de la pagina 
function url_inicio_login(){

    return site_url();
}

add_filter('login_headerurl','url_inicio_login');

function titulo_login_url(){
    return "Volver al inicio";
}
add_filter('login_headertitle', 'titulo_login_url');
// END ENQUEUE PARENT ACTION
//-------------------------------------------------
